globals;

isUpperBody = 1; %set this to 1 to get upperBodyDetections, 0 to get lowerBodyDetections.

colorset = {'y','r','r','g','g','b','b'};
if isUpperBody
    usefast = 0; %set this to 1 to get a fast detector
    if fast
        load models/upperbody_fast.mat
    else
        load models/upperbody_slow.mat
    end
    sampleDIR = 'samples/upperbody';
    numLandmarks = 7;
else
    load models/lowerbody_fast.mat
    sampleDIR = 'samples/lowerbody';
    numLandmarks = 5;
end
model.thresh = -1.0;

im = imread(fullfile(sampleDIR,'1.jpg'));
boxes = detect(im,model,model.thresh);
boxes = nms(boxes,0.5);
boxes = boxes(1,:); %we know there is a single person in the scene, so we pick the topmost detection, ignoring the rest
showboxes(im,boxes(1,:),colorset);
points = reshape(boxes(1:numLandmarks*4),4,[])';
points(points<1) = 1;
% points is a NumLandMarks x 4 matrix: each Landmark is given by one row of
% the matrix. each row is [x1 y1 x2 y2] which are the x,y coordinates of
% the top left and bottom right vertices of the rectangle containing the
% landmark.