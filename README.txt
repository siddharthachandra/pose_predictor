This code uses pretrained deformable part models for pose detection on MOBOT Data.

0. Setting up

From within matlab:
>>>compile;

1. Running

From within matlab:
>>>demo;

The demo code shows you how to load a model, get the detections, visualize detections, and get a matrix of landmark coordinates.

We have two models (fast,and slow) for the upperbody pose estimation. The 'slow' model is more accurate but takes more time.
If pose estimation time isn't important and results for the lower body pose estimation are bad, a 'slow'/more accurate model for the lower body can be trained. Please let me know if this is required.

Author:
Siddhartha Chandra
siddhartha.chandra@inria.fr

